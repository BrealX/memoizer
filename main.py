"""
app:main
"""

import asyncio

from async_lru import alru_cache

from decorators import memoize


@alru_cache(maxsize=128)
async def start_async(*args):
    """
    TODO: Add docstring
    """
    result = []
    for arg in args:
        reverse = arg[::-1]
        result.append(reverse)
    return f'The result is: {result}'


@memoize
async def start_another_async(*args):
    """
    TODO: Add docstring
    """
    result = []
    for arg in args:
        reverse = arg[::-1]
        result.append(reverse)
    return f'The result is: {result}'


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    try:
        # run async function for the first time
        a = loop.run_until_complete(start_async('foo', 'bar'))
        # print result
        print(a)
        # get cache info
        print(start_async.cache_info())

        # run the same function with the same params again
        # now the result is not calculated but is got from the cache
        b = loop.run_until_complete(start_async('foo', 'bar'))
        print(b)
        print(start_async.cache_info())
        # clear the cache
        start_async.cache_clear()

        # run the function again, the cache was reset and is filled again
        c = loop.run_until_complete(start_async('foo', 'bar'))
        print(c)
        print(start_async.cache_info())

        # run custom memoizer
        d = loop.run_until_complete(start_another_async('foo', 'baz'))
        print(d)
        # look inside the decorated function cache
        print(start_another_async.__closure__[0].cell_contents)
    except KeyboardInterrupt:
        pass
    finally:
        loop.close()
