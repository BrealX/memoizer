"""
app:decorators
"""
import asyncio
from functools import wraps


def memoize(func):
    """
    TODO: Add docstring
    """
    cache = dict()

    @wraps(func)
    async def memoized_func(*args):
        """
        TODO: Add docstring
        """
        if args in cache:
            print('this result is driven by cache')
            return cache[args]

        if asyncio.iscoroutinefunction(func):
            result = await func(*args)
        else:
            result = func(*args)
        print('this result is driven by function')

        cache[args] = result
        return result

    return memoized_func
